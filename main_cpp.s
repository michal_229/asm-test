	.file	"main_cpp.cpp"
	.intel_syntax noprefix
	.globl	array1
	.bss
	.align 32
	.type	array1, @object
	.size	array1, 48
array1:
	.zero	48
	.section	.rodata
.LC0:
	.string	"Hello World"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	sub	rsp, 288
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-8], rax
	xor	eax, eax
	mov	edi, 96
	call	_Znam
	mov	QWORD PTR [rbp-280], rax
	mov	DWORD PTR [rbp-284], 0
.L3:
	cmp	DWORD PTR [rbp-284], 31
	jg	.L2
	mov	edi, OFFSET FLAT:.LC0
	mov	eax, 0
	call	printf
	add	DWORD PTR [rbp-284], 1
	jmp	.L3
.L2:
	cmp	QWORD PTR [rbp-280], 0
	je	.L4
	mov	rax, QWORD PTR [rbp-280]
	mov	rdi, rax
	call	_ZdaPv
.L4:
	mov	eax, 0
	mov	rdx, QWORD PTR [rbp-8]
	xor	rdx, QWORD PTR fs:40
	je	.L6
	call	__stack_chk_fail
.L6:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
